import sbt._
import Keys._

object JenkinsHelperBuild extends Build {
  lazy val jenkinsHelper = Project(
    id="jenkins-helper",
    base=file(".")) settings(
    name := "jenkins-helper",
    mainClass:= Some("com.jenkins.helper.Main"),
    libraryDependencies ++= Seq(
      "com.typesafe.scala-logging" %% "scala-logging" % "3.1.0",
      "org.slf4j" % "slf4j-simple" % "1.7.13"
      )
    ) dependsOn(jenkinsDslProject)

  lazy val jenkinsDslProject =
    RootProject(uri("git://github.com/vickumar1981/jenkins-scala-dsl.git"))

}