package com.jenkins.helper

import com.jenkins.sync.dsl.jenkins.{jobs, plugins, users}
import com.jenkins.sync.dsl.JobConversions._
import com.jenkins.sync.model.{JobResult, Plugin}
import com.jenkins.sync.util._

import com.typesafe.scalalogging.LazyLogging

object Main extends LazyLogging {

  lazy val systemPythonPackages = List("ansible", "fabric")

  private def checkForPersonalRepos(jobPattern: Option[String] = None,
                                     sendRepoEmails:Option[List[JobResult] => Any] = None) =
    jobs.run(_.xml.checkGitUrls, jobPattern, !_.success, sendRepoEmails)
  private def wipeJobWorkspaces(jobPattern: Option[String] = None) =
    jobs.run(_.workspace.clean, jobPattern)
  private def jobsList(jobPattern: Option[String]) =
    if (jobPattern.isDefined) jobs(jobPattern.get) else jobs()
  private def pluginsList(pluginPattern: Option[String]) =
    if (pluginPattern.isDefined) plugins(pluginPattern.get) else plugins()

  private def listJobNames(jobPattern: Option[String] = None) =
    jobsList(jobPattern).foreach { j => logger.info("%s: %s".format(j.name, j.url)) }
  private def listPlugins(pluginPattern: Option[String] = None) = {
    val plugins = pluginsList(pluginPattern).sortBy(_.shortName)
    var totalPlugins = 0
    plugins.foreach {
      p => logger.info("%s: %s".format(p.shortName, p.version))
    }
    logger.info("Total # of plugins found: %s".format(plugins.size.toString))
  }

  private def listJobXmlConfigs(jobPattern: Option[String] = None) =
    jobsList(jobPattern).foreach { j => logger.info("%s:\n%s\n\n".format(j.name, j.xml().getOrElse(""))) }
  private def searchJobShellCommand(searchPattern: Option[String] = None) =
    jobs.run(j => {
      val shellCommand = (j.xml().get \\ "command").text.stripMargin
      if (searchPattern.isEmpty ||
        searchPattern.get.r.findFirstIn(shellCommand.replaceAll("\n", " ")).isDefined)
        JobResult(j, true, List("\n\n%s\n\n".format(shellCommand)))
      else JobResult(j, false, List("Search text not found."))
    }, None, _.success)

  private def listPythonPackages(jobPattern: Option[String] = None,
                                 checkSystemPackages: Boolean = false, sendEmails: Boolean = false) = {
    val sendVenvEmails: Option[List[JobResult] => Any] =
      if (sendEmails) Some(users.sendNoVenvNeededEmail) else None
    jobs.run(j => {
      val shellCommand = (j.xml().get \\ "command").text.stripMargin
      if (shellCommand.contains("virtualenv")) {
        val packages = "pip\\s+install\\s+(\\w)+".r.findAllIn(shellCommand).map {
          p => p.replaceAll("pip install", "").trim
        }.filterNot { p => p.toLowerCase.contains("requirements.txt") }
          .map { s => s.split(" ").toList }.flatten.toList
        if (packages.nonEmpty)
          if (checkSystemPackages)
            JobResult(j, packages.forall(systemPythonPackages.contains(_)), packages)
          else JobResult(j, true, packages)
        else JobResult(j, false, List("No virtualenv defined."))
      }
      else JobResult(j, false, List("No virtualenv defined."))
    }, jobPattern, _.success, sendVenvEmails)
  }

  def main(args: Array[String]) {
    logger.info("\n")
    checkConfiguration(args)
    val jobPattern = if (args.length > 2) Some(args(2)) else None
    args(1).toLowerCase match {
      case "check-urls" => checkForPersonalRepos(jobPattern, Some(users.sendInvalidReposEmails))
      case "check-venv" => listPythonPackages(jobPattern, true, true)
      case "grep-sh" => searchJobShellCommand(jobPattern)
      case "ls-jobs" => listJobNames(jobPattern)
      case "ls-plugins" => listPlugins(jobPattern)
      case "ls-urls" => checkForPersonalRepos(jobPattern)
      case "ls-venv" => listPythonPackages(jobPattern)
      case "ls-xml" => listJobXmlConfigs(jobPattern)
      case "wipe-ws" => wipeJobWorkspaces(jobPattern)
    }
  }

  private def checkConfiguration(args: Array[String]) = {
    require((args.length == 2) || (args.length == 3),
      """
        |The jenkins-helper requires TWO or THREE arguments:
        | the config file, the command, and an optional pattern to match job names with.
        |Ex., sbt "run [config file] [command] [pattern]"
        |
        |List of commands:
        |
        |check-urls - checks job configs for invalid git urls, sends email to git users
        |grep-sh - searches job shell commands for the specified pattern
        |ls-jobs - lists all jobs
        |ls-urls - checks jobs configs for invalid git urls, doesn't send email to git users
        |ls-xml - lists all job xml'
        |wipe-ws - wipes all job workspaces (unless a job pattern is specified)
        |ls-venv - lists jobs with python virtualenvs
        |
      """.stripMargin)
    JenkinsHelperConfig.loadConfig(args(0))
  }
}
